import { BrowsePeopleComponent } from './browse-people/browse-people.component';
import { BrowseImagesComponent } from './browse-images/browse-images.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RegistrationComponent } from './registration/registration.component';
import { AddPhotoComponent } from './add-photo/add-photo.component';
import { AddPeopleComponent } from './add-people/add-people.component';
import { FolderInfoComponent } from './folder-info/folder-info.component';
import { ImageInfoComponent } from './image-info/image-info.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, pathMatch: 'full'},
  { path: 'home', component: HomePageComponent},
  { path: 'registration', component: RegistrationComponent},
  { path: 'addPhoto', component: AddPhotoComponent},
  { path: 'browsePhoto', component: BrowseImagesComponent},
  { path: 'addPeople', component: AddPeopleComponent},
  { path: 'browsePeople', component: BrowsePeopleComponent},
  { path: 'folder/:id', component: FolderInfoComponent},
  { path: 'image/:id', component: ImageInfoComponent},
  { path: '**', component: HomePageComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
