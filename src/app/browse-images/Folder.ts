export class Folder {
  public id: string;
  public name: string;
  public description: string;
  public date_time: string;
  public specialDescription: string;
}
