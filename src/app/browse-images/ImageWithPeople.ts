import { Person } from './../add-people/Person';
import { Image } from './Image';
export class ImageWithPeople {
  public img: Image;
  public listOfPeople: Person[];
}
