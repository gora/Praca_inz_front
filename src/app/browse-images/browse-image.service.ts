import { Image } from './Image';
import { Token } from '../login/Token';
import { Folder } from './Folder';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginService } from '../login/login.service';
import { Observable } from 'rxjs';
import { ImageWithPeople } from './ImageWithPeople';
import { Confirm } from '../registration/Confirm';
import { ImageWithPeopleXY } from './ImageWithPeopleXY';

@Injectable({
  providedIn: 'root'
})
export class BrowseImageService {

  token: String;
  constructor(private http: HttpClient, private httpLogin: LoginService) { }

  selFolders(): Observable<Array<Folder>> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    return this.http.get<Array<Folder>>('https://www.sgitc.pl:8443/rest/folders/getFolders', { headers: headersForInvitesAPI });
  }

  postFolder(folder: Folder): Observable<Confirm> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    return this.http.post<Confirm>('https://www.sgitc.pl:8443/rest/folders/addFolder', folder, { headers: headersForInvitesAPI });
  }

  getImageForFolder(id: String): Observable<Array<Image>> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    return this.http.get<Array<Image>>('https://www.sgitc.pl:8443/rest/file/getImages/' + id, { headers: headersForInvitesAPI });
  }

  getFolderId(folder: Folder): Observable<Confirm> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    return this.http.post<Confirm>('https://www.sgitc.pl:8443/rest/file/getImages/', folder, { headers: headersForInvitesAPI });
  }

  getImageWithPeopleForFolder(id: String): Observable<Array<ImageWithPeople>> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    // tslint:disable-next-line:max-line-length
    return this.http.get<Array<ImageWithPeople>>('https://www.sgitc.pl:8443/rest/file/getImagesAndPeople/' + id, { headers: headersForInvitesAPI });
  }

  deleteFolderById(id: String): Observable<Confirm> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    return this.http.delete<Confirm>('https://www.sgitc.pl:8443/rest/folders/deleteFolder/' + id, { headers: headersForInvitesAPI });
  }

  deleteConnectionFromImage(peop: string, img: string): Observable<Confirm> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    const formData: FormData = new FormData();
    formData.append('peop', peop);
    formData.append('img', img);
    // tslint:disable-next-line:max-line-length
    return this.http.post<Confirm>('https://www.sgitc.pl:8443/rest/connImgPeo/deletePeopFromImgById', formData, { headers: headersForInvitesAPI });
  }

  deleteImageById(id: String): Observable<Confirm> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    return this.http.delete<Confirm>('https://www.sgitc.pl:8443/rest/file/deleteImgById/' + id, { headers: headersForInvitesAPI });
  }

  editImage(title: string, description: string, specialDescription: string, id: string): Observable<Confirm> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    const formData: FormData = new FormData();
    formData.append('title', title);
    formData.append('description', description);
    formData.append('specialDescription', specialDescription);
    formData.append('id', id);
    return this.http.put<Confirm>('https://www.sgitc.pl:8443/rest/file/updateImage', formData, { headers: headersForInvitesAPI });

  }

  editFolder(folder: Folder): Observable<Confirm> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    return this.http.put<Confirm>('https://www.sgitc.pl:8443/rest/folders/updateFolder', folder, { headers: headersForInvitesAPI });
  }

  getFolder(id: String): Observable<Folder> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    return this.http.get<Folder>('https://www.sgitc.pl:8443/rest/folders/getFolderById/' + id, { headers: headersForInvitesAPI });
  }

  getImageById(id: String): Observable<ImageWithPeople> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    return this.http.get<ImageWithPeople>('https://www.sgitc.pl:8443/rest/file/getImageById/' + id, { headers: headersForInvitesAPI });
  }

  getImageByIdXY(id: String): Observable<ImageWithPeopleXY> {
    this.token = this.httpLogin.token;
    const headersForInvitesAPI = new HttpHeaders({ 'Authorization': 'Token ' + this.token });
    return this.http.get<ImageWithPeopleXY>('https://www.sgitc.pl:8443/rest/file/getImageByIdXY/' + id, { headers: headersForInvitesAPI });
  }

}
