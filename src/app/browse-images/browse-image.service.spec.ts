import { TestBed, inject } from '@angular/core/testing';

import { BrowseImageService } from './browse-image.service';

describe('BrowseImageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BrowseImageService]
    });
  });

  it('should be created', inject([BrowseImageService], (service: BrowseImageService) => {
    expect(service).toBeTruthy();
  }));
});
