import { Person } from './../add-people/Person';
import { Image } from './Image';
import { PersonXY } from '../image-info/PersonXY';
export class ImageWithPeopleXY {
  public img: Image;
  public listOfPeople: PersonXY[];
}
