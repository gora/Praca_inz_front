export class Image {
  public id: string;
  public fileName: string;
  public fileDownloadUri: string;
  public fileType: string;
  public size: Number;
  public title: string;
  public description: string;
  public specialDescription: string;
  public folder: string;
}
