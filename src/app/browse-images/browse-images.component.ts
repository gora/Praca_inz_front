import { BrowsePeopleService } from './../browse-people/browse-people.service';
import { ImageWithPeople } from './ImageWithPeople';
import { Router } from '@angular/router';
import { Folder } from './Folder';
import { Component, OnInit, Input, Output } from '@angular/core';
import { LoginService } from '../login/login.service';
import { BrowseImageService } from './browse-image.service';
import { Image } from './Image';
import { Person } from '../add-people/Person';

@Component({
  selector: 'app-browse-images',
  templateUrl: './browse-images.component.html',
  styleUrls: ['./browse-images.component.css']
})
export class BrowseImagesComponent implements OnInit {

  // id to go to url
  idFolUrl: string;
  msgEditAlert: string;
  showImg: boolean;
  clickAddFolder: boolean;
  searchPeople: boolean;
  firstPage: boolean;
  updateImage: boolean;
  updateFolder: boolean;
  httpLogin: LoginService;
  folderList: Folder[] = [];
  httpFolder: BrowseImageService;
  folder: Folder;
  token: String;
  @Input() date: String;
  @Input() name: String;
  @Input() description: String;
  @Input() specialDescription: String;
  imgList: Image[] = [];
  imgListWithPeople: ImageWithPeople[] = [];
  msg: String;
  @Output() nameFolder: string;
  @Output() descFolder: string;
  @Output() dateFolder: string;
  @Output() specialDescFolder: string;

  allListImgPeop: ImageWithPeople[] = [];
  listOfPeople: Person[] = [];
  @Input() modelPeople: Person;
  searchListImg: ImageWithPeople[] = [];

  // edit img
  @Input() titleEdit: string;
  @Input() descriptionEdit: string;
  @Input() imgEdit: String;
  @Input() imgSpecialDescriptionEdit: string;
  idPhoto: string;

  // edit folder
  @Input() nameFolderEdit: string;
  @Input() descFolEdit: string;
  @Input() dateFolderEdit: Date;
  @Input() specialDescriptionFolderEdit: string;
  idFolderEdit: string;
  folderEdit: Folder;

  // deleteee
  idFolderDelete: string;
  idImageDelete: string;
  idConnectImgDelete: string;
  idConnectPeopDelete: string;

  constructor(private route: Router, http: LoginService, httpFolder: BrowseImageService, private httpPeople: BrowsePeopleService) {
    this.httpLogin = http;
    this.httpFolder = httpFolder;
    this.folder = new Folder();
    this.folderEdit = new Folder();
    this.msgEditAlert = '';
  }

  ngOnInit() {
    if (this.httpLogin.token === '') {
    this.route.navigate(['login']);
    }
    this.clickAddFolder = false;
    this.showImg = false;
    this.searchPeople = false;
    this.updateImage = false;
    this.updateFolder = false;
    this.firstPage = true;
    this.token = this.httpLogin.token;
    this.getFolders();
    this.getPeople();
  }

  getFolders() {
    this.httpFolder.selFolders().subscribe(res => {
      this.folderList = res;
      this.getAllImages(res);
    });
  }

  addFolderClick() {
    this.clickAddFolder = true;
    this.showImg = false;
    this.searchPeople = false;
    this.firstPage = false;
    this.updateImage = false;
    this.updateFolder = false;
  }

  showImage() {
    this.clickAddFolder = false;
    this.showImg = true;
    this.searchPeople = false;
    this.firstPage = false;
    this.updateImage = false;
    this.updateFolder = false;
  }

  search() {
    this.clickAddFolder = false;
    this.showImg = false;
    this.searchPeople = true;
    this.firstPage = false;
    this.updateImage = false;
    this.updateFolder = false;
    this.getPhotoWithPeople();
  }


  updateFol(id, nameE, desc, spec, date) {
    this.clickAddFolder = false;
    this.showImg = false;
    this.searchPeople = false;
    this.firstPage = false;
    this.updateImage = false;
    this.updateFolder = true;
    console.log(this.updateFolder);
    console.log(id);
    console.log(nameE);
    console.log(desc);
    console.log(date);
    this.nameFolderEdit = nameE;
    this.descFolEdit = desc;
    this.dateFolderEdit = date;
    this.specialDescriptionFolderEdit = spec;
    this.idFolderEdit = id;
  }

  putFolder() {
    if (this.nameFolderEdit !== null && this.nameFolderEdit !== undefined) {
      if (this.nameFolderEdit === ' ' || this.nameFolderEdit === '  ' || this.nameFolderEdit === '   ' || this.nameFolderEdit === '    ') {
        this.nameFolderEdit = 'spacja';
      }
      this.msgEditAlert = '';
      this.folderEdit.id = this.idFolderEdit;
      this.folderEdit.name = this.nameFolderEdit;
      this.folderEdit.description = this.descFolEdit;
      this.folderEdit.date_time = String(this.dateFolderEdit);
      this.folderEdit.specialDescription = this.specialDescriptionFolderEdit;
      this.httpFolder.editFolder(this.folderEdit).subscribe(ans => {
        if (ans.text === 'Edytowano') {
          this.msg = 'Zedytowano';
          this.getFolders();
        } else {
          this.msg = 'Nie udalo się zedytować folderu';
        }
      });
    } else {
      this.msgEditAlert = 'Nazwa jest wymagana';
    }

  }

  getPhotoWithPeople() {
    this.searchListImg = [];
    for (const img of this.allListImgPeop) {
      for (const pers of img.listOfPeople) {
        console.log(pers.nickname + '  ' + this.modelPeople.nickname);
        if (pers.nickname === this.modelPeople.nickname) {
          this.searchListImg.push(img);
        }
      }
    }
  }

  addFolder(name, description, special, date) {
    if (name !== null && name !== undefined ) {
      if (name === ' ' || name === '  ' || name === '   ' || name === '    ') {
        name = 'spacja';
      }
      this.msgEditAlert = '';
      this.folder.name = name;
      this.folder.description = description;
      this.folder.date_time = String(date);
      this.folder.specialDescription = special;
      this.postFold();
    } else {
      this.msgEditAlert = 'Nazwa jest wymagana';
    }

  }

  postFold() {
    this.httpFolder.postFolder(this.folder).subscribe(ans => {
      if (ans.text === 'Dodano') {
        this.msg = 'Dodano folder';
        this.getFolders();
        this.name = null;
        this.date = null;
        this.description = null;
        this.specialDescription = null;
      } else {
        this.msg = 'Nie udalo się dodać folderu';
      }
    });
  }

  getImages(id, name, desc, special, date) {
    this.showImage();
    this.nameFolder = name;
    this.descFolder = desc;
    this.dateFolder = date;
    this.specialDescFolder = special;
    this.idFolUrl = id;
    this.getFileWithPeople(id);
  }

  getFile(id) {
    this.httpFolder.getImageForFolder(id).subscribe( ans => {
      this.imgList = ans;
      console.log(this.imgList);
    });
  }

  getFileWithPeople(id) {
    this.httpFolder.getImageWithPeopleForFolder(id).subscribe(ans => {
      this.imgListWithPeople = ans;
    });
  }

  getAllImages(folderlist) {
    for (const folder of folderlist) {
      this.httpFolder.getImageWithPeopleForFolder(folder.id).subscribe(list => {
        for (const img of list) {
          this.allListImgPeop.push(img);
        }
      });
    }
  }

  getPeople() {
    this.httpPeople.getPeople().subscribe(list => {
      this.listOfPeople = list;
    });
  }

  deleteFolder(id) {
    this.idFolderDelete = id;
  }

  deleteFolder2() {
    this.httpFolder.deleteFolderById(this.idFolderDelete).subscribe(ans => {
      console.log(ans);
    });
    this.ngOnInit();
  }

  goToFolderInfo(id) {
    this.route.navigate(['/folder', id]);
  }

  goToImageInfo(id) {
    this.route.navigate(['/image', id]);
  }
}
