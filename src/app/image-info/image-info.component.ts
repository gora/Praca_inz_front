import { BrowseImageService } from './../browse-images/browse-image.service';
import { Image } from './../browse-images/Image';
import { Component, OnInit, Input } from '@angular/core';
import { LoginService } from '../login/login.service';
import { Router, ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { ImageWithPeople } from '../browse-images/ImageWithPeople';
import { ImageWithPeopleXY } from '../browse-images/ImageWithPeopleXY';
import { BrowsePeopleService } from '../browse-people/browse-people.service';
import { Person } from '../add-people/Person';
import { AddPhotoService } from '../add-photo/add-photo.service';

@Component({
  selector: 'app-image-info',
  templateUrl: './image-info.component.html',
  styleUrls: ['./image-info.component.css']
})
export class ImageInfoComponent implements OnInit {

  // img: ImageWithPeople;
  img: ImageWithPeopleXY;
  idImg: string;

  // conn id
  idPeopToDelete: string;
  idImgConn: string;

  msg: string;
  showImg: boolean;
  updateImage: boolean;
  idImageDelete: string;
  idImageEdit: string;
  peopleList: Person[] = [];


  // dane do edytowania
  @Input() titleEdit: string;
  @Input() specialDescriptionEdit: string;
  @Input() descriptionEdit: string;
  @Input() imgEdit: string;
  @Input() modelPeople: Person;

  myImg: any;

  xCon: any;
  yCon: any;
  showMarkPanel: boolean;


  // tslint:disable-next-line:max-line-length
  constructor(private httpPhoto: AddPhotoService, private httpLogin: LoginService, private httpPeople: BrowsePeopleService, private http: BrowseImageService, private router: Router, private route: ActivatedRoute) {
    this.modelPeople = new Person();
    this.showMarkPanel = false;
  }

  ngOnInit() {
    this.showImg = true;
    this.updateImage = false;
    this.showMarkPanel = false;
    if (this.httpLogin.token === '') {
      this.router.navigate(['/login']);
    } else {
      this.route.params.pipe(
        map((params) => {
          this.idImg = String(params);
          console.log(this.idImg);
          return params.id;
        }),
        switchMap(id => this.http.getImageByIdXY(id))).subscribe(image => {
          console.log(image);
          this.img = image;
          this.httpPeople.getPeople().subscribe(people => {
            this.peopleList = people;
          });
          this.myImg = document.getElementById('imgHtml');
          // let imgHtml;
          // const ammount = [];
          // for (const imgXY of this.img.listOfPeople) {
          //   ammount.push(document.getElementById('textEx'));
          // }
          // const text = document.getElementById('textEx');

          // let i = 0;
          // for (const imgXY of this.img.listOfPeople) {
          //   if (imgXY.x !== null && imgXY.x !== undefined) {
          //   // text.textContent = imgXY.person.nickname;
          //   // text.style.left = imgXY.x  + 'px';
          //   // text.style.top = imgXY.y + 'px';
          //   ammount[i].textContent = imgXY.person.nickname;
          //   ammount[i].style.left = imgXY.x  + 'px';
          //   ammount[i].style.top = imgXY.y + 'px';
          //   console.log(imgXY);
          //   i++;
          // }

          // }
        });
    }

  }

  showMarkBox() {
    this.showImg = true;
    this.updateImage = false;
    this.showMarkPanel = true;
  }

  cancel() {
    this.showImg = true;
    this.showMarkPanel = false;
    this.updateImage = false;
  }

  showMark(img) {
    // console.log(img.x);
    if (img.x !== 'null' && img.x !== undefined && img.x !== null && img.x !== 'undefined') {
      const text = document.getElementById('textEx');
      // console.log(img.x);
      text.textContent = 'x ' + img.person.nickname;
      text.style.left = img.x + 'px';
      text.style.top = img.y + 'px';
    }
  }

  // setNameOnImg(x, y) {
  //   var ctx = document.getElementById('imgHtml').msGetInputContext("2d");
  // }

  deleteImage(id) {
    this.idImageDelete = id;
  }

  markPeople() {
    console.log(this.img.img.id + '--' + this.xCon + ',' + this.yCon + ' - ' + this.modelPeople.nickname);
    this.httpPhoto.addConnectXY(this.img.img.id, this.modelPeople.id, this.xCon, this.yCon).subscribe(odp => {
      console.log(odp);
      this.xCon = null;
      this.yCon = null;
      this.http.getImageByIdXY(this.img.img.id).subscribe(image => {
        console.log(image);
        this.img = image;
      });
    });
  }

  hideMark() {
    this.showMarkPanel = false;
  }

  deleteImage2() {
    this.http.deleteImageById(this.idImageDelete).subscribe(ans => {
      console.log(ans);
    });
    // this.ngOnInit();
  }

  deleteConnection(peop, img) {
    this.idPeopToDelete = peop;
    this.idImgConn = img;
  }

  deleteConnection2() {
    this.http.deleteConnectionFromImage(this.idImgConn, this.idPeopToDelete).subscribe(ans => {
      if (ans.text === 'Edytowano') {
        this.msg = 'Usunieto';
        this.router.navigate(['/browsePhoto']);
      } else {
        this.msg = 'Nie udalo się usunac zdjęcia';
      }
    });
    this.ngOnInit();
  }

  updateImg(id, title, description, specialDescription, url) {
    this.showImg = false;
    this.updateImage = true;
    this.showMarkPanel = false;

    this.titleEdit = title;
    this.descriptionEdit = description;
    this.specialDescriptionEdit = specialDescription;
    this.imgEdit = url;
    this.idImageEdit = id;
  }

  updatePhoto() {
    console.log(this.titleEdit + '\n' + this.descriptionEdit + '\n' + this.specialDescriptionEdit + '\n' + this.idImageEdit);
    this.http.editImage(this.titleEdit, this.descriptionEdit, this.specialDescriptionEdit, this.idImageEdit).subscribe(ans => {
      if (ans.text === 'Edytowano') {
        this.msg = 'Zedytowano';
        this.img.img.title = this.titleEdit;
        this.img.img.description = this.descriptionEdit;
        this.img.img.specialDescription = this.specialDescriptionEdit;
        this.showImg = true;
        this.updateImage = false;
        this.showMarkPanel = false;
      } else {
        this.msg = 'Nie udalo się zedytować zdjęcia';
      }
    });
  }

  xyCoon(event) {
    console.log('X ->', event.clientX, 'Y ->', event.clientY);
  }

  findPosition(oElement) {
    if (typeof (oElement.offsetParent) !== undefined) {
      let posX;
      let posY;
      for (posX = 0, posY = 0; oElement; oElement = oElement.offsetParent) {
        posX += oElement.offsetLeft;
        posY += oElement.offsetTop;
      }
      return [posX, posY];
    } else {
      return [oElement.x, oElement.y];
    }
  }

  getCoordinates(e) {
    let PosX = 0;
    let PosY = 0;
    let ImgPos;
    ImgPos = this.findPosition(this.myImg);
    if (e.pageX || e.pageY) {
      PosX = e.pageX;
      PosY = e.pageY;
    } else if (e.clientX || e.clientY) {
      PosX = e.clientX + document.body.scrollLeft
        + document.documentElement.scrollLeft;
      PosY = e.clientY + document.body.scrollTop
        + document.documentElement.scrollTop;
    }
    PosX = PosX - ImgPos[0];
    PosY = PosY - ImgPos[1];
    this.xCon = PosX;
    this.yCon = PosY;
    console.log(this.xCon + ', ' + this.yCon);
  }

}
