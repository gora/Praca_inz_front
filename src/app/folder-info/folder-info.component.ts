import { BrowseImageService } from './../browse-images/browse-image.service';
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';
import {map, switchMap} from 'rxjs/operators';
import { Folder } from '../browse-images/Folder';

@Component({
  selector: 'app-folder-info',
  templateUrl: './folder-info.component.html',
  styleUrls: ['./folder-info.component.css']
})
export class FolderInfoComponent implements OnInit {

  folder: Folder;

  constructor(private httpLogin: LoginService, private http: BrowseImageService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    if (this.httpLogin.token === '') {
      this.router.navigate(['/login']);
    } else {
      this.route.params.pipe(
        map((params) => {
          return params.id;
        }),
        switchMap(id => this.http.getFolder(id))).subscribe(folder => {
        console.log(folder);
        this.folder = folder;
      });
    }
  }

}
