import { LoginService } from './../login/login.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Person } from './Person';
import { Observable } from '../../../node_modules/rxjs';
import { Confirm } from '../registration/Confirm';

@Injectable({
  providedIn: 'root'
})
export class AddPeopleService {

  constructor(private http: HttpClient, private token: LoginService) { }

  addPerson(person: Person): Observable<Confirm> {
    const header = new HttpHeaders({'Authorization': 'Token ' + this.token.token});
    return this.http.post<Confirm>('https://www.sgitc.pl:8443/rest/people/addPeople', person, {headers: header});
  }
}
