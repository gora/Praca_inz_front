import { LoginService } from './../login/login.service';
import { AddPeopleService } from './add-people.service';
import { Component, OnInit, Input } from '@angular/core';
import { Person } from './Person';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-people',
  templateUrl: './add-people.component.html',
  styleUrls: ['./add-people.component.css']
})
export class AddPeopleComponent implements OnInit {

  @Input() nickname: String;
  @Input() firstname: String;
  @Input() lastname: String;
  @Input() birthday: String;
  @Input() deathday: String;
  @Input() description: String;
  @Input() specialDescription: string;
  person: Person;
  http: AddPeopleService;
  msg: string;
  msgEditAlert: string;

  constructor(http: AddPeopleService, private httpLogin: LoginService, private route: Router) {
    this.person = new Person();
    this.http = http;
    this.msgEditAlert = '';
  }

  ngOnInit() {
    if (this.httpLogin.token === '') {
      this.route.navigate(['login']);
    }
  }

  addPeople(nickname, firstname, lastname, birthday, deathday, description, specialDescription) {
    if (nickname !== null && nickname !== undefined && nickname !== 'null' && nickname !== 'undefined') {
      if (nickname === ' ' || nickname === '  ' || nickname === '   ' || nickname === '    ') {
        nickname = 'spacja';
      }
      this.msgEditAlert = '';
      console.log(specialDescription);
      this.person.firstname = firstname;
      this.person.nickname = nickname;
      this.person.birthday = String(birthday);
      this.person.lastname = lastname;
      this.person.deathday = String(deathday);
      this.person.description = description;
      this.person.specialDescription = specialDescription;
      this.addPerson();
    } else {
      this.msgEditAlert = 'Nie podano pseudonimu!';
    }

  }

  addPerson() {
    this.http.addPerson(this.person).subscribe(ans => {
      // console.log(ans.text);
      if (ans.text === 'Dodano') {
        this.msg = 'Dodano osobę';
        this.firstname = null;
        this.nickname = null;
        this.birthday = null;
        this.lastname = null;
        this.deathday = null;
        this.description = null;
        this.specialDescription = null;
      } else {
        this.msg = 'Nie udalo się dodać osoby';
      }
    });
  }

}
