import { TestBed, inject } from '@angular/core/testing';

import { AddPeopleService } from './add-people.service';

describe('AddPeopleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddPeopleService]
    });
  });

  it('should be created', inject([AddPeopleService], (service: AddPeopleService) => {
    expect(service).toBeTruthy();
  }));
});
