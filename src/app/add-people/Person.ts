export class Person {
  public id: string;
  public nickname: string;
  public firstname: String;
  public lastname: String;
  public birthday: String;
  public deathday: String;
  public description: String;
  public specialDescription: string;
}
