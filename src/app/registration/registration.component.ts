import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RegistrationService } from './registration.service';
import { User } from './User';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  http: RegistrationService;
  user: User;
  msg: String;
  msgTr: boolean;
  @Input() name: String;
  @Input() surname: String;
  @Input() email: String;
  @Input() password: String;
  msgEditAlert: string;


  constructor(private route: Router, http: RegistrationService) {
    this.http = http;
    this.user = new User();
    this.msgEditAlert = '';
   }

  ngOnInit() {
    this.msgTr = false;
  }

  addUser(user) {
    this.http.register(user).subscribe( res => {
      console.log(res.text);
      this.msg = res.text;
      if (this.msg === 'Nie udalo sie zarejestrować') {
        this.msgTr = false;
      } else {
        this.msgTr = true;
      }

    });
  }

  register(name, surname, email, password) {
    if (email !== null && email !== undefined) {
      this.msgEditAlert = '';
      if (password !== null && password !== undefined) {
        this.msgEditAlert = '';
        this.user.name = name;
        this.user.surname = surname;
        this.user.email = email;
        this.user.password = password;
        this.addUser(this.user);
      } else {
        this.msgEditAlert = 'Nie podano hasła!';
      }
    } else {
      this.msgEditAlert = 'Nie podano loginu!';
    }

  }

  closeModel() {
    if (this.msgTr === true) {
      this.route.navigate(['login']);
    }
  }
}
