import { Confirm } from './Confirm';
import { Injectable } from '@angular/core';
import { User } from './User';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient) { }

  register(user: User): Observable<Confirm> {
    return this.http.post<Confirm>('https://www.sgitc.pl:8443/user/register', user);
  }
}
