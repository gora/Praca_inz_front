import { LoginService } from './../login/login.service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Confirm } from '../registration/Confirm';

@Injectable({
  providedIn: 'root'
})
export class AddPhotoService {

  constructor(private http: HttpClient, private login: LoginService) { }

  addFile(file: File, title: string, description: string, specialDescription: string, folder: string): Observable<string> {
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Token ' + this.login.token});
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('title', title);
    formData.append('description', description);
    formData.append('specialDescription', specialDescription);
    formData.append('folder', folder);

    return this.http.post<string>('https://www.sgitc.pl:8443/rest/file/uploadFile', formData, {headers: headersForInvitesAPI});
  }


  getFileId(file: File, title: string, description: string, specialDescription: string,  folder: string): Observable<String> {
    const headersForInvitesAPI = new HttpHeaders({'Authorization': 'Token ' + this.login.token});
    const formData: FormData = new FormData();
    formData.append('file', file);
    formData.append('title', title);
    formData.append('description', description);
    formData.append('specialDescription', specialDescription);
    formData.append('folder', folder);

    return this.http.post<String>('https://www.sgitc.pl:8443/rest/file/getImgId', formData, {headers: headersForInvitesAPI});
  }

  addConnect(img: string, peop: string): Observable<Confirm> {
    const formData: FormData = new FormData();
    formData.append('img', img);
    formData.append('peop', peop);
    const header = new HttpHeaders({'Authorization': 'Token ' + this.login.token});
    return this.http.post<Confirm>('https://www.sgitc.pl:8443/rest/connImgPeo/addConnect', formData, {headers: header});
  }

  addConnectXY(img: string, peop: string, x: string, y: string): Observable<Confirm> {
    const formData: FormData = new FormData();
    formData.append('img', img);
    formData.append('peop', peop);
    formData.append('x', x);
    formData.append('y', y);
    const header = new HttpHeaders({'Authorization': 'Token ' + this.login.token});
    return this.http.post<Confirm>('https://www.sgitc.pl:8443/rest/connImgPeo/addConnectXY', formData, {headers: header});
  }



}
