import { Person } from './../add-people/Person';
import { BrowsePeopleService } from './../browse-people/browse-people.service';
import { Folder } from './../browse-images/Folder';
import { BrowseImageService } from './../browse-images/browse-image.service';
import { Observable } from 'rxjs';
import { FileData } from './FileData';
import { Component, OnInit, Input } from '@angular/core';
import { AddPhotoService } from './add-photo.service';
import { saveAs } from '../../../node_modules/file-saver/src/FileSaver';
import { LoginService } from '../login/login.service';
import { Router } from '../../../node_modules/@angular/router';
import { Mark } from './Mark';

@Component({
  selector: 'app-add-photo',
  templateUrl: './add-photo.component.html',
  styleUrls: ['./add-photo.component.css']
})
export class AddPhotoComponent implements OnInit {
  imgUrl: String = '/assets/images/imgUpload.jpg';
  fileToUpload: File;
  http: AddPhotoService;
  fileAdd: FileData;
  @Input() title: string;
  @Input() description: string;
  @Input() specialDescription: string;
  folderList: Folder[] = [];
  peopleList: Person[] = [];
  @Input() modelFolder: Folder;
  @Input() modelPeople: Person;
  addedPeople: Person[] = [];
  markListPeople: Array<Mark> = [];
  mark: Mark;
  imgId: string;
  msg: string;
  inneFolderId: string;
  myImg: any;

  xCon: any;
  yCon: any;

  msgEditAlert: string;

  // tslint:disable-next-line:max-line-length
  constructor(http: AddPhotoService, private httpFolder: BrowseImageService, private httpPeople: BrowsePeopleService, private httpLogin: LoginService, private route: Router) {
    this.fileAdd = new FileData();
    this.modelPeople = new Person();
    this.modelFolder = new Folder();
    this.mark = new Mark();
    this.http = http;
    this.msgEditAlert = '';
  }

  ngOnInit() {
    if (this.httpLogin.token === '') {
      this.route.navigate(['login']);
    } else {
      this.httpFolder.selFolders().subscribe(folder => {
        this.folderList = folder;
        this.findFolderInne();
      });
      this.httpPeople.getPeople().subscribe(people => {
        this.peopleList = people;
      });
      this.myImg = document.getElementById('myImgId');
    }

  }

  findFolderInne() {
    for (const folder of this.folderList) {
      if (folder.name === 'Inne') {
        this.inneFolderId = folder.id;
      }
    }
  }

  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);

    const reader = new FileReader();
    reader.onload = (event: any) => {
      this.imgUrl = event.target.result;
    };
    reader.readAsDataURL(this.fileToUpload);
  }

  addPhoto(folder) {
    if (this.fileToUpload !== null && this.fileToUpload !== undefined) {
      this.msgEditAlert = '';
      this.fileAdd.file = this.fileToUpload;
      this.fileAdd.title = this.title;
      this.fileAdd.description = this.description;
      this.fileAdd.specialDescription = this.specialDescription;
      if (this.modelFolder.id !== undefined) {
        this.fileAdd.folder = this.modelFolder.id;
      } else {
        this.fileAdd.folder = this.inneFolderId;
      }
      this.addImg();
    } else {
      this.msgEditAlert = 'Nie wybrano zdjęcia!';
    }


  }

  addImg() {
    // tslint:disable-next-line:max-line-length
    this.http.addFile(this.fileAdd.file, this.fileAdd.title, this.fileAdd.description, this.fileAdd.specialDescription, this.fileAdd.folder).subscribe(ans => {
      console.log(ans);
      this.imgId = ans;
      if (this.addedPeople.length > 0) {
        this.addMarkPeople(ans);
      } else if (ans !== null) {
        this.msg = 'Dodano';
        this.fileToUpload = null;
        this.title = '';
        this.description = '';
        this.specialDescription = '';
        this.modelFolder.id = null;
        this.inneFolderId = null;
      } else {
        this.msg = 'Cos poszlo nie tak';
      }
    });
  }

  // chooseFolder(folder) {
  //   console.log(this.modelFolder.id);
  // }

  // chooseFolder1(folder) {
  //   console.log(this.modelPeople.id);
  // }

  markPeople() {
    // if (this.modelFolder.id !== undefined) {
      this.addedPeople.push(this.modelPeople);

      if (this.modelPeople !== undefined) {
      this.markListPeople.push({person: this.modelPeople, x: this.xCon, y: this.yCon});
      }
      this.xCon = null;
      this.yCon = null;
      console.log(this.markListPeople);
    // }
  }

  addMarkPeople(ans) {
    for (const peop of this.markListPeople) {
      console.log(peop);
      // this.http.addConnect(ans, peop.id).subscribe(odp => {
      //   console.log(odp);
      //   if (odp.text === 'Dodano') {
      //     this.msg = 'Dodano zdjęcie';
      //     this.fileToUpload = null;
      //     this.title = null;
      //     this.description = null;
      //     this.specialDescription = null;
      //     this.modelFolder.id = null;
      //     this.imgUrl = '/assets/images/imgUpload.jpg';
      //     this.addedPeople = [];
      //     this.modelPeople = null;
      //     this.modelFolder = null;
      //   } else {
      //     this.msg = 'Nie udalo się dodać zdjęcia';
      //   }
      // });

      this.http.addConnectXY(ans, peop.person.id, peop.x, peop.y).subscribe(odp => {
        console.log(odp);
        if (odp.text === 'Dodano') {
          this.msg = 'Dodano zdjęcie';
          this.fileToUpload = null;
          this.title = null;
          this.description = null;
          this.specialDescription = null;
          this.modelFolder.id = null;
          this.imgUrl = '/assets/images/imgUpload.jpg';
          this.addedPeople = [];
          this.modelPeople = null;
          this.modelFolder = null;
        } else {
          this.msg = 'Nie udalo się dodać zdjęcia';
        }
      });
    }
  }

  xyCoon(event) {
    console.log('X ->', event.clientX, 'Y ->', event.clientY);
  }

  findPosition(oElement) {
    if (typeof (oElement.offsetParent) !== undefined) {
      let posX;
      let posY;
      for (posX = 0, posY = 0; oElement; oElement = oElement.offsetParent) {
        posX += oElement.offsetLeft;
        posY += oElement.offsetTop;
      }
      return [posX, posY];
    } else {
      return [oElement.x, oElement.y];
    }
  }

  getCoordinates(e) {
    let PosX = 0;
    let PosY = 0;
    let ImgPos;
    ImgPos = this.findPosition(this.myImg);
    if (e.pageX || e.pageY) {
      PosX = e.pageX;
      PosY = e.pageY;
    } else if (e.clientX || e.clientY) {
      PosX = e.clientX + document.body.scrollLeft
        + document.documentElement.scrollLeft;
      PosY = e.clientY + document.body.scrollTop
        + document.documentElement.scrollTop;
    }
    PosX = PosX - ImgPos[0];
    PosY = PosY - ImgPos[1];
    this.xCon = PosX;
    this.yCon = PosY;
    console.log(this.xCon + ', ' + this.yCon);
  }
}


