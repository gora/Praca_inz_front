export class FileData {
  public file: File;
  public title: string;
  public description: string;
  public specialDescription: string;
  public folder: string;
}
