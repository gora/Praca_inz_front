import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  tokenIs: boolean;
  tokenUser: String;

  constructor(private router: Router, private token: LoginService) {
    this.tokenUser = '';
  }

  ngOnInit() {
    this.tokenIs = false;
  }

  checkToken() {
    this.tokenUser = this.token.token;
    if (this.tokenUser !== '') {
      this.tokenIs = true;
    } else {
      this.tokenIs = false;
    }
    return this.tokenIs;
  }

  logOut() {
    this.token.token = '';
    this.router.navigate(['/login']);
    document.getElementById('acc').style.width = '0vw';
    document.getElementById('acc').style.overflow = 'hidden';
    this.router.navigate(['login']);
    this.checkToken();
  }
}




