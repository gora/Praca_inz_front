import { RegistrationService } from './registration/registration.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LoginComponent } from './login/login.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RegistrationComponent } from './registration/registration.component';
import { AddPhotoComponent } from './add-photo/add-photo.component';
import { BrowseImagesComponent } from './browse-images/browse-images.component';
import { HttpClientModule, HttpClient, HttpParams} from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { LoginService } from './login/login.service';
import { BrowseImageService } from './browse-images/browse-image.service';
import { AddPeopleComponent } from './add-people/add-people.component';
import { BrowsePeopleComponent } from './browse-people/browse-people.component';
import { FolderInfoComponent } from './folder-info/folder-info.component';
import { ImageInfoComponent } from './image-info/image-info.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    HomePageComponent,
    RegistrationComponent,
    AddPhotoComponent,
    BrowseImagesComponent,
    AddPeopleComponent,
    BrowsePeopleComponent,
    FolderInfoComponent,
    ImageInfoComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    HttpModule
  ],
  providers: [LoginService, BrowseImageService, RegistrationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
