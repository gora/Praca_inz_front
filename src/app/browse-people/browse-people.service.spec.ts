import { TestBed, inject } from '@angular/core/testing';

import { BrowsePeopleService } from './browse-people.service';

describe('BrowsePeopleService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BrowsePeopleService]
    });
  });

  it('should be created', inject([BrowsePeopleService], (service: BrowsePeopleService) => {
    expect(service).toBeTruthy();
  }));
});
