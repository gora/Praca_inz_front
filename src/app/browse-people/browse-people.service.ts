import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LoginService } from '../login/login.service';
import { Person } from '../add-people/Person';
import { Confirm } from '../registration/Confirm';

@Injectable({
  providedIn: 'root'
})
export class BrowsePeopleService {

  token: String;
  constructor(private http: HttpClient, private httpLogin: LoginService) { }

  getPeople(): Observable<Array<Person>> {
    this.token = this.httpLogin.token;
    const header = new HttpHeaders({'Authorization': 'Token ' + this.token});
    return this.http.get<Array<Person>>('https://www.sgitc.pl:8443/rest/people/getPeople', {headers: header});
  }

  putPeople(person: Person): Observable<Array<Person>> {
    this.token = this.httpLogin.token;
    const header = new HttpHeaders({'Authorization': 'Token ' + this.token});
    return this.http.put<Array<Person>>('https://www.sgitc.pl:8443/rest/people/updatePeople', person, {headers: header});
  }

  delete(id: string): Observable<Confirm> {
    this.token = this.httpLogin.token;
    const header = new HttpHeaders({'Authorization': 'Token ' + this.token});
    return this.http.delete<Confirm>('https://www.sgitc.pl:8443/rest/people/delete/' + id, { headers: header });
  }
}
