import { LoginService } from './../login/login.service';
import { Person } from './../add-people/Person';
import { Component, OnInit, Input } from '@angular/core';
import { BrowsePeopleService } from './browse-people.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-browse-people',
  templateUrl: './browse-people.component.html',
  styleUrls: ['./browse-people.component.css']
})
export class BrowsePeopleComponent implements OnInit {

  http: BrowsePeopleService;
  peopleList: Person[] = [];
  showSearchPeople: Person[] = [];
  @Input() modelPeople: Person;
  editPerson: Person;
  @Input() id: string;
  @Input() nickname: String;
  @Input() firstname: String;
  @Input() lastname: String;
  @Input() birthday: String;
  @Input() deathday: String;
  @Input() description: String;
  @Input() specialDescription: string;
  showImg: boolean;
  showEdit: boolean;
  deleteId: string;
  msgEditAlert: string;

  constructor(http: BrowsePeopleService, private httpLogin: LoginService, private route: Router) {
    this.http = http;
    this.editPerson = new Person();
    this.msgEditAlert = '';
  }

  ngOnInit() {
    this.showImg = true;
    this.showEdit = false;
    if (this.httpLogin.token === '') {
      this.route.navigate(['login']);
    }
    this.getListOfPeople();
  }

  getListOfPeople() {
    this.showImg = true;
    this.showEdit = false;
    this.http.getPeople().subscribe(list => {
      this.peopleList = list;
      this.showSearchPeople = list;
      // console.log(this.peopleList);
      // this.getPhotoWithPeople();
    });
  }

  cancel() {
    this.showImg = true;
    this.showEdit = false;
  }

  getListOfPeopleSearch() {
    this.showImg = true;
    this.showEdit = false;
    this.http.getPeople().subscribe(list => {
      this.peopleList = list;
      this.showSearchPeople = list;
      // console.log(this.peopleList);
      this.getPhotoWithPeople();
    });
  }

  search() {
    this.showImg = true;
    this.showEdit = false;
    this.getListOfPeopleSearch();
    // this.getPhotoWithPeople();
  }

  getPhotoWithPeople() {
    this.showSearchPeople = [];
    for (const people of this.peopleList) {
        console.log(people.nickname + '  ' + this.modelPeople.nickname);
        if (people.nickname === this.modelPeople.nickname) {
          this.showSearchPeople.push(people);
        }
    }
  }

  showAll() {
    this.getListOfPeople();
    this.showImg = true;
    this.showEdit = false;
    this.showSearchPeople = this.peopleList;
  }

  showUpdate(id, nickname, firstname, lastname, birthday, deathday, description, specialDescription) {
    this.showImg = false;
    this.showEdit = true;

    this.editPerson.id = id;
    this.nickname = nickname;
    this.firstname = firstname;
    this.lastname = lastname;
    this.birthday = birthday;
    this.deathday = deathday;
    this.description = description;
    this.specialDescription = specialDescription;

  }

  update(nickname, firstname, lastname, birthday, deathday, description, specialDescription) {
    // this.editPerson.id = id;
    console.log(this.editPerson);
    if (nickname !== null && nickname !== undefined && nickname !== 'null' && nickname !== 'undefined') {
    if (nickname === ' ' || nickname === '  ' || nickname === '   ' || nickname === '    ') {
      nickname = 'spacja';
    }
    this.msgEditAlert = '';
    this.editPerson.nickname = nickname;
    this.editPerson.firstname = firstname;
    this.editPerson.lastname = lastname;
    this.editPerson.birthday = birthday;
    this.editPerson.deathday = deathday;
    this.editPerson.description = description;
    this.editPerson.specialDescription = specialDescription;

    // console.log(this.editPerson);
    this.updatePerson();
  } else {
    this.msgEditAlert = 'Nie podano pseudonimu!';
  }
  }

  updatePerson() {
    this.http.putPeople(this.editPerson).subscribe(ans => {
    });
  }

  deletePeople(id) {
    this.deleteId = id;
  }

  delete2() {
    this.http.delete(this.deleteId).subscribe(ans => {
      console.log(ans);
      this.getListOfPeople();
    });
  }
}
