import { Token } from './Token';
import { Injectable } from '@angular/core';
import { Login } from './Login';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  token: String = '';
  email: String = '';
  constructor(private http: HttpClient) { }

  login(login: Login): Observable<Token> {
    return this.http.post<Token>('https://www.sgitc.pl:8443/user/token', login);
  }
}
