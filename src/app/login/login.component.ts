import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { Login } from './Login';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  http: LoginService;
  loginUser: Login;
  loginWrong: boolean;
  @Input() email: String;
  @Input() password: String;

  constructor(private router: Router, http: LoginService) {
    this.loginUser = new Login();
    this.http = http;
  }

  ngOnInit() {
    this.loginWrong = false;
  }

  registration() {
    this.router.navigate(['registration']);
  }

  getToken() {
    this.http.login(this.loginUser).subscribe(token => {
      if (token.token != null) {
        this.loginWrong = false;
        // console.log(token);
        this.http.token = token.token;
        this.http.email = this.email;
        document.getElementById('emailText').innerHTML = String(this.email);
        document.getElementById('acc').style.width = 'auto';
        document.getElementById('acc').style.overflow = 'visible';
        this.router.navigate(['browsePhoto']);
      } else {
        this.loginWrong = true;
      }
    });
  }

  logIn(email, password) {
    this.loginUser.email = email;
    this.loginUser.password = password;

    this.getToken();
  }

}
